import { CommonSystemInfo, getRangeDistanceFromBand } from "../systeminfo.js";
import { getAllItemsInWorld } from "../helpers.js";

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class Ironclaw2EItemSheet extends ItemSheet {

    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["ironclaw2e", "sheet", "item"],
            width: 720,
            height: 600,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }

    /** @override */
    get template() {
        const path = "systems/ironclaw2e/templates/item";
        // Return a single sheet for all item types.
        //return `${path}/item-sheet.html`;

        // Alternatively, you could use the following return statement to do a
        // unique item sheet by type, like `weapon-sheet.html`.
        return `${path}/item-${this.item.data.type}-sheet.html`;
    }

    /* -------------------------------------------- */

    /** @override */
    getData() {
        const baseData = super.getData();
        let sheetData = {};
        // Insert the basics
        sheetData.item = baseData.data;
        sheetData.data = baseData.data.data;

        // Insert necessary misc data
        sheetData.options = baseData.options;
        sheetData.cssClass = baseData.cssClass;
        sheetData.editable = baseData.editable;
        sheetData.limited = baseData.limited;
        sheetData.title = baseData.title;
        sheetData.dtypes = baseData.dtypes;

        // Add structural sheet stuff
        let selectables = { "handedness": CommonSystemInfo.equipHandedness, "range": CommonSystemInfo.rangeBands, "giftOptions": CommonSystemInfo.giftSpecialOptions, "giftStates": CommonSystemInfo.giftWorksStates };
        sheetData.selectables = selectables;
        sheetData.showDirectoryOptions = game.user.isGM && !this.item.parent;
        sheetData.rangeDistance = getRangeDistanceFromBand(sheetData.data.range);

        return sheetData;
    }

    /* -------------------------------------------- */

    /** @override */
    setPosition(options = {}) {
        const position = super.setPosition(options);
        const sheetBody = this.element.find(".sheet-body");
        const bodyHeight = position.height - 192;
        sheetBody.css("height", bodyHeight);
        return position;
    }

    /* -------------------------------------------- */

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;

        // Gift special handlers 
        html.find('.add-new-special').click(this._onAddNewSpecial.bind(this));
        html.find('.delete-special-option').click(this._onDeleteSpecial.bind(this));
        html.find('.copy-special-settings').click(this._onCopySpecialSettings.bind(this));
        html.find('.copy-all-aspects').click(this._onCopyAllAspects.bind(this));

        html.find('.change-setting-mode').change(this._onChangeSpecialOption.bind(this));
        html.find('.special-change-field').change(this._onChangeSpecialField.bind(this));
        html.find('.special-change-number').change(this._onChangeSpecialNumber.bind(this));
        html.find('.special-change-boolean').change(this._onChangeSpecialBoolean.bind(this));
    }

    /**
     * Handle the addition of a new special option
     * @param {Event} event Originationg event
     */
    _onAddNewSpecial(event) {
        this.item.giftAddSpecialSetting();
    }

    /**
     * Handle the deletion of a special option
     * @param {Event} event Originationg event
     */
    _onDeleteSpecial(event) {
        const li = $(event.currentTarget).parents(".special-option");
        const index = li.data("special-index");
        this.item.giftDeleteSpecialSetting(index);
        //li.slideUp(200, () => this.render(false));
    }

    /**
     * Handle the copying of Special Settings
     * @param {Event} event Originationg event
     */
    _onCopySpecialSettings(event) {
        if (game.user.isGM) {
            // Pop a dialog to confirm
            let confirmed = false;
            let dlog = new Dialog({
                title: game.i18n.localize("ironclaw2e.dialog.copyItem.title"),
                content: `
     <form>
      <h2>${game.i18n.format("ironclaw2e.dialog.copyItem.copySpecial", { "name": this.item.name })}</h2>
     </form>
     `,
                buttons: {
                    one: {
                        icon: '<i class="fas fa-check"></i>',
                        label: game.i18n.localize("ironclaw2e.dialog.copy"),
                        callback: () => confirmed = true
                    },
                    two: {
                        icon: '<i class="fas fa-times"></i>',
                        label: game.i18n.localize("ironclaw2e.dialog.cancel"),
                        callback: () => confirmed = false
                    }
                },
                default: "one",
                render: html => { },
                close: async html => {
                    if (confirmed) { // Only copy these settings and replace existing ones if confirmed
                        const gifts = getAllItemsInWorld("gift");
                        gifts.delete(this.item);
                        for (let gift of gifts) {
                            if (gift.name === this.item.name) {
                                console.log(gift); // Log all potential changes to console, just in case
                                await gift.update({ "data.specialSettings": this.item.data.data.specialSettings });
                            }
                        }
                    }
                }
            });
            dlog.render(true);
        }
    }

    /**
     * Handle the copying of item data
     * @param {Event} event Originationg event
     */
    _onCopyAllAspects(event) {
        const data = this.item.data.data;
        if (game.user.isGM) {
            // Pop a dialog to confirm
            let confirmed = false;
            let dlog = new Dialog({
                title: game.i18n.localize("ironclaw2e.dialog.copyItem.title"),
                content: `
     <form>
      <h2>${game.i18n.format("ironclaw2e.dialog.copyItem.copyAll", { "name": this.item.name })}</h2>
     </form>
     `,
                buttons: {
                    one: {
                        icon: '<i class="fas fa-check"></i>',
                        label: game.i18n.localize("ironclaw2e.dialog.copy"),
                        callback: () => confirmed = true
                    },
                    two: {
                        icon: '<i class="fas fa-times"></i>',
                        label: game.i18n.localize("ironclaw2e.dialog.cancel"),
                        callback: () => confirmed = false
                    }
                },
                default: "one",
                render: html => { },
                close: async html => {
                    if (confirmed) { // Only copy the item data and replace existing ones if confirmed
                        const items = getAllItemsInWorld(this.item.type);
                        items.delete(this.item);
                        for (let item of items) {
                            if (item.name === this.item.name) {
                                console.log(item); // Log all potential changes to console, just in case
                                await item.update({ "data": data });
                            }
                        }
                    }
                }
            });
            dlog.render(true);
        }
    }

    /**
     * Handle the change of a setting mode
     * @param {Event} event Originationg event
     */
    _onChangeSpecialOption(event) {
        event.preventDefault();
        const li = $(event.currentTarget).parents(".special-option");
        const index = li.data("special-index");
        const option = event.currentTarget.value;
        this.item.giftChangeSpecialSetting(index, option);
    }

    /**
     * Handle change in a text field special setting
     * @param {any} event
     */
    _onChangeSpecialField(event) {
        event.preventDefault();
        const li = $(event.currentTarget).parents(".special-option");
        const index = li.data("special-index");
        const name = event.currentTarget.name;
        const value = event.currentTarget.value;
        this.item.giftChangeSpecialField(index, name, value);
    }

    /**
     * Handle change in a number field special setting
     * @param {any} event
     */
    _onChangeSpecialNumber(event) {
        event.preventDefault();
        const li = $(event.currentTarget).parents(".special-option");
        const index = li.data("special-index");
        const name = event.currentTarget.name;
        const value = parseInt(event.currentTarget.value);
        if (typeof value !== "number") return;
        this.item.giftChangeSpecialField(index, name, value);
    }

    /**
     * Handle change in a boolean special setting
     * @param {any} event
     */
    _onChangeSpecialBoolean(event) {
        event.preventDefault();
        const li = $(event.currentTarget).parents(".special-option");
        const index = li.data("special-index");
        const name = event.currentTarget.name;
        const value = event.currentTarget.checked;
        this.item.giftChangeSpecialField(index, name, value);
    }
}
